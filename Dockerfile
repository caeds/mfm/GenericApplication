# Base image.
FROM alpine

# Copy files into image 
COPY . /root/Git/GitLab/GenericApplication 

# Add image layers (as necessary)
# (Modify the code as appropriate for container environment)
# (Here we set the paths to dependencies on the container)
RUN echo -e "#\!/bin/bash" > $HOME/Git/GitLab/GenericApplication/SharedResources/paths.sh
RUN echo -e "export VIDEO_SRC_PATH=\"\$HOME/media/data/videos/raw\" " >> $HOME/Git/GitLab/GenericApplication/SharedResources/paths.sh 
RUN echo -e "export VIDEO_DEST_PATH=\"\$HOME/media/data/videos/obfuscated\" " >> $HOME/Git/GitLab/GenericApplication/SharedResources/paths.sh   
RUN echo -e "mkdir -p \$VIDEO_SRC_PATH; mkdir -p \$VIDEO_DEST_PATH " >> $HOME/Git/GitLab/GenericApplication/SharedResources/paths.sh
# Establish the relevant path directories in the container
RUN source /root/Git/GitLab/GenericApplication/SharedResources/paths.sh
# (Optional) Run the application (must always be preceeded by execution of paths.sh!)
#RUN source /root/Git/GitLab/GenericApplication/MainApplication/obfuscate.sh
